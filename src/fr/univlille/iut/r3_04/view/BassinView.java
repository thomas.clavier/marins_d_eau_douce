package fr.univlille.iut.r3_04.view;

import fr.univlille.iut.r3_04.controlleur.TerminalControl;
import fr.univlille.iut.r3_04.model.Bassin;
import fr.univlille.iut.r3_04.model.ECoup;
import fr.univlille.iut.r3_04.utils.Observer;
import fr.univlille.iut.r3_04.utils.Subject;

public class BassinView implements Observer {

	protected Bassin bassin;
	protected TerminalControl controleur;
	protected boolean triche;
	
	public BassinView(Bassin bassin) {
		this(bassin, false);
	}
	
	public BassinView(Bassin bassin, boolean triche) {
		this.triche = triche;
		this.bassin = bassin;
		bassin.attach(this);

		triche();

		controleur = new TerminalControl(bassin);
		controleur.run();
	}

	protected void triche() {
		if (triche) {
			show();
		}
	}

	protected void show() {
		showAxeX();

		for (int y=0; y < bassin.getHauteur(); y++) {
			ytick(y);
			for (int x=0; x < bassin.getLargeur(); x++) {
				switch (bassin.getCase(y,x).getEtat()) {
				case EAU :    System.out.print(" "); break;
				case BATEAU : System.out.print("o"); break;
				case TOUCHE : System.out.print("*"); break;
				}
			}
			ytick(y);
			System.out.println();
		}
		showAxeX();
	}

	protected void showAxeX() {
		System.out.print( ' ');
		for (int x=0; x < bassin.getLargeur(); x++) {
			xtick(x);
		}
		System.out.println();
	}

	protected void xtick(int x) {
		tick(x,'|');
	}

	protected void ytick(int y) {
		tick(y,'-');
	}

	protected void tick(int i, char tick) {
		if ( i % 5 == 0) {
			System.out.print( tick);
		} else {
			System.out.print( '.');
		}
	}

	@Override
	public void update(Subject subj) {
		update(subj, bassin.getDernierCoup());
	}

	@Override
	public void update(Subject subj, Object dernierCoup) {
		triche();

		switch ((ECoup)dernierCoup) {
		case DEBUT :    System.out.println("Jouez le premier coup"); break;
		case TOUCHE :   System.out.println("Bateau touché"); break;
		case COULE :    System.out.println("Bateau coulé"); break;
		case A_L_EAU :  System.out.println("Un coup à l'eau"); break;
		case ERREUR :   System.out.println("Tir erroné"); break;
		
		case FINI :
			System.out.println("Bravo c'est gagné");
			controleur.setFin();
			break;
		}
	}
}
