package fr.univlille.iut.r3_04.model;

public enum EBateau {
	PORTE_AVION(5), CROISEUR(4), ESCORTEUR(3), SOUS_MARIN(2), TORPILLEUR(1);
	
	protected int taille;

	private EBateau(int taille) {
		this.taille = taille;
	}
	
	public int getTaille() {
		return taille;
	}
}
