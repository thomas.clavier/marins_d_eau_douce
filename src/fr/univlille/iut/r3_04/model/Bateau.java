package fr.univlille.iut.r3_04.model;

public class Bateau {
	protected Case[] elements;
	protected Flotte flotte;

	public Bateau(EBateau type, int x, int y, boolean horizontal, Bassin bassin) {
		this.elements = new Case[type.getTaille()];
		this.flotte = bassin.getFlotte();

		if(horizontal){
			for(int i=0; i < type.getTaille(); i++){
				elements[i]=bassin.getCase(x+i,y);
			}
		}else{
			for(int i=0; i < type.getTaille(); i++){
				elements[i]=bassin.getCase(x,y+i);
			}
		}
		
		for (Case elt : elements) {
			elt.setEtat(ECase.BATEAU);
		}
	}

	public boolean contient(int x, int y){
		for(Case elt : elements){
			if (elt.equals(x,y)) {
				return true;
			}
		}
		return false;
	}

	public boolean estDetruit(){
		boolean detruit = true;
		// si une des cases n'est pas TOUCHE, 'detruit' devient (et reste) false
		for (Case elt : elements){
			detruit = detruit && (elt.getEtat() == ECase.TOUCHE);
		}
		return detruit;
	}

}
